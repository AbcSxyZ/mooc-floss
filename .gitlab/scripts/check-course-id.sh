#!/bin/bash

# This files is shown when a MR is done that fails the CI because of an
# unchecked course ID

URL_NAME="2021_1"
ORG="MOOC-FLOSS"
COURSE="101"

WRONG_URL_NAME="$(grep -ho 'url_name=\"[^\"]*\"' course/course.xml | cut -d \" -f 2)"
WRONG_ORG="$(grep -ho 'org=\"[^\"]*\"' course/course.xml | cut -d \" -f 2)"
WRONG_COURSE="$(grep -ho 'course=\"[^\"]*\"' course/course.xml | cut -d \" -f 2)"

echo "
This commit modifies the course ID!
(it should be course-v1:$ORG+$COURSE+$URL_NAME
    but it is course-v1:$WRONG_ORG+$WRONG_COURSE+$WRONG_URL_NAME)

To rectify these problems, please run the scripts/rename-course.sh
script from this repository.

Thanks!"
