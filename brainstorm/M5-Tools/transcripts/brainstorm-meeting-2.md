# Brainstorm - Module 5 - Meeting 2 Transcript

Video: https://digimedia1.r2.enst.fr/playback/presentation/2.0/playback.html?meetingId=73d3cd53ec8ede3f636b9a782ecc9f6375cf2271-1620665925750

Marc: 00:00:00.748 Yeah. Okay.

Xavier: 00:00:03.628 And then to become part of the group I think anonymity is a good rule because if there is anyone who is uncomfortable with someone new joining, I think it's generally worth prioritizing the existing group which only new people should be fairly consensual. It should be quite obvious once someone is working out. But again, that's one of the ways of doing it, so.

Marc: 00:00:29.003 Yep. I'm mostly fine with it. I think we can even have a lighter process; that is, if someone approves the merge requests, and no one disagrees in a week, and can be merged anyway.

Xavier: 00:00:45.403 Yeah. That can encourage people to review actually. Yeah, that's a good idea.

Marc: 00:00:50.367 Yep. And they could make much faster in that case because we know that if it has been approved for a week, then that means that no one disagrees with it.

Xavier: 00:01:02.971 Yep. That works.

Marc: 00:01:08.214 But we should actually write it down in a file somewhere.

Remi: 00:01:13.790 Is it part of Git workflow somehow or, I mean, because even this kind of process should be explained a little bit in our course.

Marc: 00:01:26.344 It's not specific to Git. It's basically how do people decide what's done and what's not done.

Remi: 00:01:33.984 So it should discussed in the course?

Xavier: 00:01:37.242 Yeah. Actually, I've added a point for that. You'll see in my pull request from Saturday because on week four, we discussed a lot the code of conduct, but that's actually kind of part of this type of processes, so I did a point for the contributing that MD in that section so that we discuss that. And I've also added a point that for all the documents that we talk about for all the projects, we should try to have that in our project, in the MOOC itself, to make sure that they can see that; they can maybe have a first step to look at the course before looking at another project or something like that. So that will remind us also to do that if we haven't right now.

Remi: 00:02:31.367 Do you know any graphical resource to see the processes of the different workflows for Git or the different states for merge requests? Because in GitLab, it could be not approved, approved, under discussion. I don't know, merged or not.

Marc: 00:02:56.920 We could make some infographics, or we could have some infographics made for that, probably in the week about GitLab and stuff like that.

Xavier: 00:03:09.316 Okay. Makes sense. Yeah. And I don't know of an existing reference for this, but it's not possible that it would exist. I mean, there are not a lot of different states. Generally open, merge, and closed are the three ones, and it's generally self-explanatory, but it might still be good for people to know that because it's true that it's obvious once you've seen a few, but you might not know even, for example that - I don't know - merge requests get refused. It might not be completely of use given the number of people who just open something and imagine that their work is done, so yeah.

Remi: 00:03:50.305 Yeah. You have this example from Polliard, the pull request that is, I mean, downvoted by so many people they decided not to merge it, which is a great use case I think to study.

Xavier: 00:04:14.721 Yep, and I think the pull request is from the project maintenance, right, sort of journey, right?

Marc: 00:04:20.711 Yes.

Xavier: 00:04:21.833 So it shows also actually even and maybe especially for the project maintenance to get to a point where you've already written the code, but the community disagrees. It shows that they did something wrong and it should have been discussed earlier, really.

Marc: 00:04:37.269 That is sort of what they admitted then that they should have communicated more clearly and received back before. But they also said that the-- and I tend to agree that the reaction to a work in progress merge request, not a ready merge request that has been decided too much, was a bit a strong reaction, but for telemetry or network stuff in general, it's a fairly standard reaction. Having a work-in-progress merge request does not mean that the code will be there. Even if from the maintenance, it's really likely.

Remi: 00:05:23.685 So today, we discuss-- all the time, I'll a little bit annoyed that we still are using week as a [family?]. We should completely modify everything and say modules or chapter or I don't know, not weeks because I don't want to have timing in our course.

Marc: 00:05:51.734 Sure, so module 5.

Remi: 00:05:54.232 Yeah. Okay.

Marc: 00:05:56.137 Module 5is about tools.

Remi: 00:06:01.305 Okay. So for module 5, we have the transcript. Xavier, I know that you did fantastic job doing all of the synthesis of the brainstorms. Did you do that for week five? I mean module 5, sorry.

Xavier: 00:06:26.892 No. I have not. I may cut around on Saturday to start doing it for module 4, and I still haven't done actually the second brainstorm.

Remi: 00:06:36.484 Yeah, it takes really a long time. And you do it [crosstalk]--

Xavier: 00:06:39.716 Every--

Remi: 00:06:40.354 --also.

Marc: 00:06:41.637 I've read, Xavier, module 5 transcript like half an hour ago, so I can tell you what it is about.

Xavier: 00:06:49.961 Good thing.

Marc: 00:06:50.853 So we basically discussed the tools around projects and the diversity of tools involved around project. But most of the discussions were about how do we get students to actually contribute something to an open-source project, like very small things, which was, is there a typo or a documentation fix? And the other stuff we discussed was how to make sure that the students could actually compile/test the in-development code of the project they want to contribute to. So basically, there may be a wild diversity of tools involved up in open-source projects, depending on the language, depending on the workflow, depending on lots of things, like depending whether it's in Python, inside Django code, or C++ application or whatever.

Marc: 00:07:52.386 And we wanted to sort-- we talked about providing basic info for different kinds of projects. Like if it's a Django project, we will probably need sort of these tools. If it's a C++ project, these tools might be more adapted, but most of the time, we want to teach how to find a contributing guide basically. So how to find the information you will need to compile and test your project. And the other thing is that contributing guide will allow you to find the information you'll need to contribute the fix. So if you have found a typo in the code, or if you have found that by following the guide you hit your problems, and you will need to contribute a fix to the contributing guide, and that will be your contribution.

Marc: 00:08:54.335 So that basically what we want as a goal for this week is to have as a student report on the GitLab that they have successfully built and tested the development state of the project they want to contribute to. And maybe ideally, to have contributed to a typo, or something like that, or a documentation fix like anything minor to the project so that they know that they can propose something and have a working discussion channel with the project that can help them fix before starting on a bigger issue like a code issue. And that's it for this summary.

Remi: 00:09:42.798 Okay. And the challenge is to reproduce a minor bug from the bug tracker. That was one of the activities-- challenge we wanted to put.

Marc: 00:10:04.081 Yes, that too.

Xavier: 00:10:08.411 Yeah-- sorry, go ahead.

Marc: 00:10:11.512 So two things basically, building the development status of the project, and reproduce a bug from a bug tracker on just development state of the project.

Remi: 00:10:21.185 That's the, I think, the most challenging module of the whole MOOC. Yes, I think it is because there is such a wide diversity of tooling, and a lot of errors are happening when you try to set up everything.

Xavier: 00:10:47.008 Yeah, but I don't think that will be our work to take care of that because that will be so different from one project to the next. Even in the project itself for financing, it's one of the challenging aspects. So I think that's really something that needs to be taken care of at the project level. What we can give is like the starting point. The methodology for that to look for documentation, look for developmental alignment and station steps to ask people in the project if something works, because that's very normal. To know that it's normal for things to break like most project, like the setup it's pretty challenging. But I don't think we want to enter the dangerous realm of trying to fix that for people.

Remi: 00:11:35.597 So does it mean in the interaction between students, we try not to have something related to specific to a project, or? That would be difficult like students coming, "I don't know how to install Docker on Windows. I have a problem. Who can help me?"

Xavier: 00:12:05.150 I mean, since they will be doing that within the context of the project they have selected and since we have talked about maybe pairing students together when they don't have a mentor, for example, they can of, course, help each other. But I think we should make it clear that the right place for getting support beyond that should be the project itself. And actually, that's one of the first and best contributions they can make within the project, it's to actually notice bugs with the installation process because often, the established developers they have their own environment set up, there are changes, they don't see that it breaks, and it's only newcomers who'll get the issue. And a lot of the times, because people get issues, they would get stuck and they don't report it and it remains like that, sometimes for a while.

Xavier: 00:13:00.005 So I think showing to them that this is actually part of the contribution process, that they can help by discussing that with that community, I think we can avoid having a lot of those discussions and having to solve the problem from the whole open-source world. But there is still that group, like this pairing if people are on the same project, they can of course help each other. And another point to not forget is that we'll have prepared them in terms of social interactions for the past four modules so that one of the points of talking to people, becoming part of the community, having done a few things, having already helped a few of the people is that when they do have a question, they're more likely to get an answer. So it's also a moment where they can start using that social capital in a way to try to get solutions. They will also know who to talk to, hopefully.

Remi: 00:14:14.763 So I think Loic said, "We need not only checklist on how to build a project, etc., but rather, something that even for beginners or people who are not comfortable, we want them not to stop the MOOC because it's too difficult and they don't know what they are doing and are not sure about what to do next because it's quite diverse and stuff. And so we want them, from the very beginning of this module, to warn them it's difficult sometimes and they should really get some help." So because I'm just thinking, what should we first do in this module? Should we explain all that, that it would be difficult and usually there is a build process, testing process, tooling, etcetera, or should we do an activity? I don't know. Maybe to make them more comfortable, we can explain with an example, maybe? I don't know.

Xavier: 00:15:48.497 I think it does make sense to give a good framework just so that they expect that to take a long time, that we can guide them as much as possible at least, in times of process. And again, an activity might be a good thing because if we present that as like, "Okay, no, your next contribution to the project is going to be to kind of debug the installation process and contribute feedback or fixes for that," then they know that they are expected to have issues. Versus if we just have one line, know, install the development environment, and good luck, then obviously, the expectation would be different. So an activity would be nice.

Remi: 00:16:32.381 So is it the first time where we ask them to download the source code or should they do that before? Because in week four, we have to find the right places, like the right branch with the right fork, of the right project, blah, blah. So maybe, I don't remember if, in the module before, we asked them to download or to clone the code.

Xavier: 00:17:12.429 So I think that's actually module 2 because module two is where locations. So that's where we have, yeah.

Remi: 00:17:24.551 [crosstalk].

Xavier: 00:17:26.913 Yes. So I mean, one of the challenges is that we let people change their mind until we are module 4. But I think one of the approaches that we have with that is that [prompt?] people would select directly the right one, they don't change their mind later on, they will do that progressively each module. For the ones who change their mind later on until module 4, basically, we'll ask them to go through those steps again, so to make sure that they have actually done that. So I think if we have those steps in module 2, whether they do it on like the second week or whatever our timeframe they use, or they come back to it from module 4, I think we'll in both cases guarantee that they have done that.

Remi: 00:18:12.577 Yeah.

Polyedre: 00:18:19.444 Marc, you are muted.

Marc: 00:18:22.709 Thank you. So basically, on week two we make sure they know how to clone and because they have to do it on their report projects. But we only have in the tips about the project, the fact that they should fork and fix a typo maybe on the report they're interested in. And on module 4 we talked a lot-- no.

Remi: 00:18:55.569 But sorry, on module 2 they have to clone the dumb project we are just creating for the course, right?

Marc: 00:19:06.381 Yep.

Remi: 00:19:06.879 Not a real project. So indeed, it will be in here that we ask them to download the code or the real project for the first time, yes?

Marc: 00:19:18.553 Yes.

Xavier: 00:19:18.652 Sort of. I'm not completely sure about that, because I think it makes sense if they have already a project with in module 2 where we are talking about those things, that maybe, we start with a fake one. We often have that during the different modules where we start with lighter activities during the bulk of the theoretical teaching, in a way. And at the end, in the project tips, we say, "Okay, you know what in that safe environment? Try to apply it to that field project." So I think it could make sense to have something like that also for module 2. Because if they have already fixed a typo, or even if it's within the documentation, it doesn't necessarily need to be the code actually at that point, but generally, that would still be our Git repositories and etc. I think they will already have touched a bit those things, and again, I think within the context of looking at locations, code is in an important location because they look at merge request. They look at where the code is. Even if they haven't completely set up the development environment, that might be something more for module 5, at least having used and seen the different location would be an important thing.

Marc: 00:20:43.790 That's for a different thing we have in module 2. The 2.4 is, "Find a Typo and Fix It?" But that means that they have to check out the code, sort of. But in any case, in module 4 I think they have to have a choice of their project, which means that they need to know where the code of this project is because it's part of their checklist for a project like, know where the code is, know where the bug tracker is, etc. So they should be able to clone from the knowledge they have from week two and the knowledge they gathered about the project. But you're right that the first time, we will actually require and want them to do it at this point.

Remi: 00:21:45.283 Okay.

Marc: 00:21:46.051 But it should not be a hard part at this point.

Remi: 00:21:48.592 Yeah, yeah, yeah. Yes. So the first activity is to read the contributing guide and try to find the steps to have a correctly configured development environment setup?

Marc: 00:22:24.701 Could be, but we can also adapt to projects that don't have a contributing guide because it's implicit for the language or things they do. But most of the time, it's like, we're to make, make, make install. And sometimes, dot/configure--prefix= [don't laugh?] pwd/build. No, slash-- [laughter] I mean, I know how to write [crosstalk].

Remi: 00:22:56.123 Good luck for the transcript of that. [laughter]

Marc: 00:23:02.841 I mean you just do that, dot/configure--prefix if you plan to make install and then-- [laughter] but that's some knowledge about autoconf project that I gathered and set. Maybe written in the contributing guide, and may not because it's very standard.

Remi: 00:23:32.689 I remember that's also, Polliard, you were talking about code style, Monday.

Polyedre: 00:23:47.902 Yeah, but it was about the Mac done in the MOOCs or maybe not related to exactly this module. Just more general. Yeah, I wanted to add: if there is no contributing, maybe we can just ask first our students to identify the framework or the language plus not to be able to look for classic ways to build a project. So maybe it can be an alternative to the contributing.

Remi: 00:24:40.975 And sometimes projects in the contributing guide they don't have explanation on how to build or how to set up the dev environment. I know it sounds crazy but [laughter] sometimes it is the case. One example, I can give you give you one example.

Marc: 00:25:00.765 Most of the time it's the case, I mean.

Remi: 00:25:02.770 Okay. So you have to find--

Marc: 00:25:04.588 Development environment is developers' responsibility. I don't really care what ID you use. Use whatever suits you.

Remi: 00:25:13.987 But in any case, they have to find - maybe it's even not in the documentation - how to set up the dev environment, okay, but also how to build. And the build process could be different depending on the platform also. Let me give you two examples. The first one is element from [Kershview?]. It's a audio-- so you put a graph of audio modules and you produce real-time sound with it. So they have a building file. And they have contributing file and a code style, also. It's in different parts. Another example is--

Marc: 00:26:22.274 There is a WAF file so I know what to do.

Remi: 00:26:26.312 A what?

Marc: 00:26:28.073 [laughter] WAF, W-A-F.

Remi: 00:26:31.532 Oh my God. What is it?

Marc: 00:26:34.555 It's a build system.

Remi: 00:26:37.222 I don't know this build system.

Marc: 00:26:40.185 I think it was made by Google. But it's very arcane. It's--

Remi: 00:26:46.416 Okay.

Marc: 00:26:47.226 But if you do a PT install with WAF it's probably there. Nope. Nope.

Remi: 00:26:55.635 All right. So something will happen here. Very quickly. Sometimes it's impossible to build on Windows, clearly. And sometimes it's impossible to build on Linux. Because if it is a macOS app-- [laughter] so what should we do with this use case, with these cases like projects that are-- so let me give you an example. A very good digital, audio workstation, Ardour I think. Yes, ardour.org. So it's open source. But if you want to build it on Mac and Linux, they give you precise instructions. But if you want to build it on Windows, they don't.

Marc: 00:27:51.866 Fair enough.

Remi: 00:27:55.988 Oh, also, I don't know, we never mentioned economic model or somewhere, but I don't remember. I have a classic economic model where you have to pay for built releases.
[silence]

Polyedre: 00:28:28.709 Maybe you can just focus on the main use case. I mean, maybe 90% of products. And later, when we have the very structure of the course, and that we have some students that struggle with some products, we can amend the content to explain, to add some description about it. So the idea would be not to be exhaustive now but to update the MOOC if there is some problem in the future.

Remi: 00:29:23.106 I understand but I don't know what to do with students that cannot build the project. Either because it's impossible on their platform or it's too difficult.

Xavier: 00:29:37.839 We can encourage them to have a virtual machine on Linux.

Remi: 00:29:44.928 That's a possibility. Yes. Do you think statistically, most of the open-source projects are usually developed under Linux? Maybe not.

Marc: 00:30:18.070 Linux or macOS.
[silence]

Remi: 00:30:30.224 But statistically, we will have more Windows students. [laughter]. I think. Maybe not, but.

Marc: 00:30:38.946 Probably, yeah. I mean, what I do to get a normal development environment in Windows is to, basically, install an Arch Linux system within Windows, and that allows to sort out everything within Windows. But it's not really friendly if you don't know Arch Linux before. It's provided by your MSYS Project.

Remi: 00:31:17.741 Okay. But for example, to build Inkscape because--

Marc: 00:31:26.765 That's what we use.

Remi: 00:31:28.656 To build Inkscape for Windows, you use MSYS--

Marc: 00:31:36.750 MSYS2 which is the same thing as Mingw-64.

Remi: 00:31:41.891 Yes. Okay. So where, basically, it's a Linux system on Windows, right?

Marc: 00:31:49.778 Sort of, yeah. But some projects do have instructions to build on Visual Studio with MSVC and things like that, but even on windows we use Clang or G++.

Remi: 00:32:08.608 Okay. So most of the week, we should not give a lot of activities because it will be a lot of searching, communicating with the community, trying to download, build. That's a longer work where it really depends on the project, but I'm not sure what we should do exactly. How do they know that it was built correctly? And--

Marc: 00:32:44.731 So you can just launch it. Usually if it launches, you're good. [laughter] Even for web platforms when you have some complicated build system, at the end if you have, "Oh, your site is available on Port 8080," then you're good. You test it and if you don't have an error of 500 or something like that, you're good. I don't know exactly on OpenStack you have DevStack or something like that. That takes care of everything, I think.

Kendall: 00:33:34.075 It takes care of deployment, yeah, for probably most of the projects. Yeah. So I always run DevStack in a virtual machine because that's what you were supposed to do even if you were running a Linux machine, because it can do nasty things on your computer. So it doesn't really matter if you're running Linux windows or Mac iOS on your machine because DevStack itself will run in a virtual machine anyway, or some virtual environment or something that separates it from the local machine. And you can configure it and then it practically deploys the projects or about the services from OpenStack that you've picked. And then you will get a running OpenStack environment. So it doesn't really do much of a building because OpenStack is practically a big pile of scripts, but it does create a running environment for you that you can then play with.

Remi: 00:34:41.554 Okay.

Kendall: 00:34:45.445 And then you can ask people how much they like or dislike DevStack and whether or not they think it was a good idea to come up with it on the first place. But that's the secondary question here.

Remi: 00:34:59.376 I think it's a good idea.

Kendall: 00:35:02.668 It definitely helps a lot. It made my life easier a couple of times, but it's another topic in the sense of maintaining it and making it efficient and so forth. The other thing that might be connecting to this discussion here is with Upstream Institute, how we are calling the OpenStack Upstream training in the past couple of years, we do have a prepared virtual machine image for students who wish to download and use that. And it has Git on it and some IDs that they can see the code in and an IRC client. And we go through with them to set it up. Oh well, these days we ask them to complete that as a homework before they come to the training, and then we double-check if they do have a working environment, so that way we don't have to deal that much with what's the base operating system that they're running and so forth. So they will get kind of a clean developer environment that's a virtual machine that they can destroy, and then they can download it again if they want, and set it up again if they want a totally clean slate for whatever reason. So it can always be an option to suggest virtualization. Sometimes that might still be easier than doing stuff on your local machine.

Marc: 00:36:48.134 Yes. I agree that it's the easiest way to have a clean slate environment, but it's also some work to explain how to set up virtual machines and what can go wrong with them. I don't know, Rémy, did you already taught classes to use VMs?

Remi: 00:37:19.489 Yes, with you sometimes, with Docker and OpenStack.

Marc: 00:37:32.952 Yes, but I meant locally with VirtualBox or equivalent, because cloud machines are an alternative, but sometimes it's-- I don't know.

Remi: 00:37:46.073 Only because we had VirtualBox infrastructure at our school. Do you remember that, Marc? Well, we still have, so yeah, but really little. But I mean, it's quite user-friendly. VirtualBox is the usual way and not so difficult, and the documentation is great.

Marc: 00:38:20.530 I have several machines where I cannot run VirtualBox because the kernel module doesn't want to load anymore, but.

Remi: 00:38:29.225 Really?

Marc: 00:38:30.224 Yes.

Remi: 00:38:31.136 Oh, and on Windows also it happened to me. If you have the--

Marc: 00:38:39.253 Virtualization disabled in the BIOS you have to enable it.

Remi: 00:38:42.402 Yes, this. Yes. And also if you have the hypervisor Hyper-V that is used, for example, for version two of Windows Subsystem for Linux, then you have to disable it, restart, and then you can use VirtualBox. You cannot use VirtualBox with another Type 1 Hypervisor. So sometimes it could be quite messy to find how to just start VirtualBox on Windows, either by BIOS--

Marc: 00:39:23.520 But I agree that once installed, it's fine.

Remi: 00:39:26.091 Yes.

Marc: 00:39:31.140 So should we expect people to use VirtualBox or just suggest it to those who run Windows?

Remi: 00:39:39.214 It depends on the project. If it's a Windows-only project, then you don't care.

Xavier: 00:39:47.331 Yeah, I think to know I need the projects would have guidance on that. I know that, for example, on Open edX, it started using VirtualBox for the installation that was part of the process and switch to Docker, so now that's the recommended one. So if we were saying like, "Here's VirtualBox," people might go the wrong way if we say that. Might just say more generally, that the more isolated or contained the setup of the dev environment will be, the more they can make a mistake and start from scratch but I would not go all the way, I think.
[silence]

Marc: 00:40:35.942 So what is not an activity for this week? I'm looking at the calc.
[silence]

Remi: 00:41:10.542 Anyway, most of it will be a lot of reading documentation, searching, trying to build. And if it's impossible, then installing a VM, debugging everything. That's a lot of work already. So I think most of the content will be description of the activities and pointers to good resources that we usually use for Docker, VirtualBox, or any very common dev tool.
[silence]

Xavier: 00:42:07.184 Yes, it's up to, I believe, very general teaching here. I mean, we'll have some partner projects, so we could always have section that apply more specifically to them but actually no, even that would probably be better contributed to the project themselves. So maybe some part about how to get help when they are stuck. I mean, it's a little bit related to the previous weeks where we said to talk, where we get them to mix in with the community, how to ask smart questions, etc. But since we are getting to more technical questions, maybe we could give examples of people who are stuck. Maybe illustrate some more good and bad questions like coming to the forum and saying, "Dev stack doesn't work," is not helpful, but if you come and bring a description and etc., so we could even link to actual threads that are good examples and bad examples, things like that.
[silence]

Xavier: 00:43:45.565 And again, we also have the-- the fact that we want to present a lot of that like a potential contribution so to fix it. I mean, that will be part of the activity but we could have maybe a section that talks about technical documentation, how to write good technical documentation, which perspective to tag, the fact that as a new contributor or newcomer to the project, they have a perspective that is lacking from people who have been in the project for a longer time. So instead of being ashamed of being newcomers and not knowing anything, they should actually look at that as a precious thing. They can really contribute from that [inaudible]. Maybe again some parts about the psychological effects of why me as a newcomer who doesn't know how to do anything, should I contribute something when there are already so many good people in the project. It's psychological aspect, something they have to go through to realize that actually, after all, they are not that bad and that what is sow is actually useful two years to others. This kind of things maybe could be worth reinforcing.

Xavier: 00:45:14.403 Very good pointing, Polliard, to have a fallback. Yeah, that could be the partner projects basically where we can predict a little bit. I mean, on that side, I guess I don't know if either Open edX or OpenStack would actually be reliable in their setup of the development environment. I know that Open edX is not really very reliable with that. So maybe we could have one that is a bit easier to tell them as a fallback. I don't know. Or yeah, at least a way to reliably get help from those ones. Because the thing is that if we give them a fallback project and they are able to set up the development environment there, but then it's not the project they want to contribute to they are still stuck in that project, right? Unless you mean that in case they really switched to that one. Could be.

Kendall: 00:46:24.837 I mean giving OpenStack as a backup plan if someone who has no intention to contributing to the project, I'm not really sure how useful that is because it's quite big and complex so I really don't know what people will learn from it if they don't intend to contribute to OpenStack and they're not interested in that particular project. And on the other hand if you're installing DevStack from the master branch and you're not setting it to stable, it may or may not be stable also. So you can also not be totally 100% sure that everything will work as expected because in the middle of the release anything could be broken and then communities working on to fix it, but it's not 100% reliable that way either.

Xavier: 00:47:18.453 Yeah, it's the same on Open edX and I think any project of a significant size almost will probably have this kind of issues from time to time, so it will be difficult to point to one project and say, "Okay, this one. Yeah, it's going to be for sure easy to install."

Kendall: 00:47:34.372 Yeah, and I think we also need to ask what they are gaining from there and how deep in the weeds, we want to get in. Because, again, deploying OpenStack is one thing but if I want to contribute to something that's written in C and I have to build it and figure it out, that's really completely different. And at some point it boils down to whether if the person wants to code, and build the project, and know all those then at some point you need to just point them to the direction of learn coding first because I don't think that we want to go down into that level of details. You can't really do that in an abstract way, I wouldn't think. But I might be wrong.

Marc: 00:48:31.576 No, I agree. So at this point it's module 5 and they're supposed to have chosen a project, and if they cannot build the project-- they should at the end of this week be able to build this project, and it should be in the language that they know they can write, and they can read because they have chosen it that way. So they have chosen a language that they know so they know base tooling about it. They know if it's CUF compiler, if it's Python, you have an interpreter. This is the type of thing that we don't want to teach and that we take for granted. And I've put it in the calc as, there is difference with-- they should sort of-- we should teach the intuition of what's the difference between what they are supposed to know and what they are not supposed to know. And programming or the most basic tooling for that language are things that people are supposed to know beforehand because we are not-- I am not teaching you how to use a code editor or to edit a code file, whatever language you are using. And that's not something they should ask the projects either because they don't really care about it. And that's not something that they should spend time to answer. And, yeah, so at this point, a backup project is not a good thing, I think, because it would only make them lose time in the perspective of their contribution after.

Xavier: 00:50:13.322 Yeah. Yeah, I agree on that. Actually, I think those points are important requirement that maybe we should integrate in the checklist on the previous weeks. And maybe even from the start say, "Okay, when you pick your project, even if it's tentative, try to focus on ones where you actually know the language, or at least you're willing to learn it in the context of this course from somewhere else so that it's not a surprise on module 5 for this." There might still be cases where actually they have the requirement but they really struggle with the development environment. Can happen, so. I can perfectly imagine some people getting stuck on the DevStack from Open edX and probably OpenStack. We have some people joining the team with tech like a couple of weeks to get that appropriately set up, sometimes when it's really in a bad shape. So maybe even if we don't have a backup project, we should tell them, "Okay, if you really get stuck and you've exhausted all of those options, maybe go back to the previous weeks and just pick another project." Or something like that. That might be the ultimate.

Remi: 00:51:28.463 Maybe we should ask them to have three projects from the very beginning. The number one is the one I want really to contribute, and the number two and three I really like those applications and I'd pick them if I have any problem in the future as backup projects.

Marc: 00:51:51.876 I think that would only make things more confused. Because when we ask them to go and discuss with projects, if they have three projects to think about, they will probably discuss themselves too much.

Xavier: 00:52:08.306 Yeah, and we'll also send a lot of conflicting signals to different projects, where they will be coming, but not really contributing.

Remi: 00:52:18.017 Okay.

Kendall: 00:52:20.261 At the end of the day, engaging with a community, if somebody is serious, it takes a lot of time, and you're building your persona and everything. So it's not really about, "Oh, I want to learn how to contribute to an open-source project. This one looks cool, but that one looks cool, too, so I will just have a group of them, and then I don't do anything in either of them, just annoying people." And it's not useful for anybody, especially the person who's trying to be active and achieve something somewhere. So I would hope that when people are choosing a project, they actually do have something in mind with it. Either they made it for work, or they have a personal interest in it, and maybe we should put more effort into describing this at the beginning, that if they are not really interested in a project, then don't go there.

Remi: 00:53:10.960 Yes.

Kendall: 00:53:11.422 Because, then, what's the point? What's your motivation in it? Because just to hang out with open-source geeks, that's not really a good motivation, I wouldn't think. And "it looks good on my resume", that's not a good motivation, either. And I think that's important. I don't think we are here to make sure that everyone who got a wild idea one day succeeds, but those people who really want this, but have no idea how, they can succeed. At least, I think that should be kind of the target pool, and anyone else who can benefit from the training and the content and the material, even better.

Remi: 00:53:57.465 Totally agree.

Marc: 00:53:59.691 Yeah. And about your problem with-- Rémy, you mentioned that even if they know the language, the tooling can cause problems. That's precisely this kind of thing that they are not supposed to know but can learn from the project, and should be in the contributing guide, if not obvious. So that's precisely what we can tell them. It's okay if you don't know the precise tooling that you need in this project, but you can ask them, because they know it, and they-- it's part of welcoming new contributors. That you explain what tooling the people are supposed to use, if it's specific. If it's not specific, then they will tell you, "Use whatever you want, and it should work." If it's specific, like DevStack, for instance, there should be a page somewhere on OpenStack website to say how to use DevStack in five command lines.

Kendall: 00:54:55.055 Yeah, and we can give suggestions, like if you have to run DevStack and it seems to be failing, then look into how you can deploy the latest stable release of OpenStack. Same thing for other projects where you have to build the code, look for the latest stable release, and try to build that first, because that should work. It should be stable, that's why it's the stable release. So we can still give people some pointers where they can figure out the tooling without being on the unstable master branch. So we can still do that without going into the deep details of how this and that project works.

Marc: 00:55:43.852 If the unstable phase, and you can try to fix it, and that will be your contribution.

Kendall: 00:55:56.121 Yeah. Communities are thankful if people jump in and help them fix tooling. You can get a lot of good credits for that.
[silence]

Remi: 00:57:10.600 Maybe we should also notify them or give an advise on how to self-regulate the time so that they don't spend too much time doing something specific. If they tried to build the thing for one day, the whole day, and they are completely stuck, maybe we should say time constraints, or I don't know.

Marc: 00:57:50.013 They can also ask help to the other people in that project in the course which might have the same problems or might have hit that problems and fixed it somehow.

Remi: 00:58:03.222 Ah, so that might be the interaction between the students in this module.

Xavier: 00:58:16.401 Maybe also something that I have noticed with some beginners with code is that sometimes the systematic process for debugging things is not completely intuitive. They might search a little bit but not necessarily have the conviction that they can actually go to the end of an issue, not necessarily have all the methodology for that. So that's not something we want to handle as part of the course because that's not part of the scope. But I wonder if we could find resources, courses, or something somewhere that can help them to acquire that. Like, "If you get really stuck follow this. I know it's a long other course or a long book or whatever but that's going to help your whole career, and definitely for this, you need to have that methodology." So maybe that could be also a way to help them. I don't know of a good one though. So if anyone does.

Remi: 00:59:27.537 I don't either. I was thinking how do we-- like the Tinder app that Olivier was speaking about, yes, how do we connect students with the same project?

Xavier: 00:59:51.603 I think we had talked at first at least use the groupwork feature of Open edX. We don't really know if that would be sufficient, and it's not at fancy as having a Tinder likewise like right when you like the project or the person to collaborate with. But that might be a easier first step because there's no code to write.

Marc: 01:00:15.744 Yeah. It seemed to be adapted to what we wanted to do.

Remi: 01:00:29.051 Or sometimes you have a big forum and all the threads are the starting point for the different projects or all the sub forums, something like that.

Marc: 01:00:44.510 Like in this course?

Remi: 01:00:46.973 Yes. Or Reddit, or.

Xavier: 01:00:52.749 Yeah. I mean, I don't remember how the groupwork handle discussions, but it might be that they are encouraged to create a forum post. We can always have a forum category for that, like open a thread for a specific project. This way maybe even between courses, they might be able to see a thread from the previous run or something, where the people had an issue and post up to that. We could.

Remi: 01:01:24.212 So if we do that, we have to explain it explicitly. Like you have to search if someone else already started a thread in the forums, etc.

Marc: 01:01:41.727 It's quarter past 8:00, so maybe it's time to finish the calc to assign people and see that we have all activities that we want to do.

Xavier: 01:02:01.904 Sure.

Marc: 01:02:04.761 So we only have activities-- ah, no, except that how to get help.

Remi: 01:02:09.874 So most of the work we'll have to do is explaining how to achieve the activity quite precisely as we can because of the diversity of the project.

Marc: 01:02:29.976 Do we want to give examples? One example in a Python project, one example in a C++ project, one example in a Java project to take some most used languages?

Xavier: 01:02:41.989 With what would you show with the example?

Marc: 01:02:45.582 I take some known project in Python and I build it. And I test that it's running, for instance.

Xavier: 01:02:55.884 Oh, you mean like on video you show how you do it or something like that?

Marc: 01:03:01.216 Yes. And then I take a C++ project, I check out it and I compile it and then I run it and I show that it works. And [crosstalk] to do Java, whatever, and then I run it.

Xavier: 01:03:16.419 But is that really going to translate well across project? Even on specific language because it's going to be very--

Marc: 01:03:25.286 I think it gives an idea of what's expected. It's like take the code, and from the code get a running program. And it shows that it's not rocket science and it's not that difficult. Of course, we are going to emphasize the fact that we know the steps because we have the contributing guide for this project, and this is easy when you know how to do it. But we want to show what's sort of expected and what they should be expected to find.

Xavier: 01:03:59.347 But wouldn't that be something we would kind of assume they're already are as analogy? If that's the first project they ever build, that's going to be a bit tricky now.

Remi: 01:04:10.757 Maybe they have--

Marc: 01:04:11.340 It could be the first project from GitLab they want to build. Also for big projects they build because most of the time, when you program things, especially in school, you just have one file, and you compile it with a building tool, for instance.

Xavier: 01:04:28.407 Yep, that's true. It's not something really specific to contributing, though, but it's true that that might be a step for people. It depends whether we want to do that because they're-- yeah. Guess why not, but my instinct would be to try to keep that out of the scope, and maybe to refer to existing things because I'm sure that-- actually, we could have videos, or maybe other projects or courses around that, more specifically, that we can refer to. Maybe videos on YouTube for specific projects. I don't know.

Remi: 01:05:04.047 Yeah, instead of not wasting time of creating these kind of resources, which we could just point to external ones if they exist. Yeah, I agree that the most important thing is to have really well-described activities and what we want them to achieve, and how we evaluate the outcome of the activity.

Marc: 01:05:43.940 See, outcomes should be like some things they add on their GitLab report of things they've done.

Remi: 01:05:54.986 And how do you check that after you evaluate it? Pure grading?

Marc: 01:06:02.392 Generating, maybe.

Remi: 01:06:10.012 Okay.

Marc: 01:06:11.220 I don't know anything better. I don't know anything else that could work for this kind of--

Remi: 01:06:17.977 Yes.

Marc: 01:06:22.106 And we could say that teachers of the course will manually check all the repos and validate what's valid and what's not valid, but it's not going to scale.
[silence]

Marc: 01:07:00.826 And then we should have precise instructions to know what to put in the report and how to evaluate it.

Remi: 01:07:08.810 Yep.
[silence]

Remi: 01:07:21.141 What do you mean when you say report about what you did in your GitLab?

Marc: 01:07:26.619 In your GitLab repo?

Remi: 01:07:28.978 Ah. Okay, in your repo, okay.

Marc: 01:07:32.912 Because in the week two, they should have created a repo where they put steps of what they've done.

Remi: 01:07:39.549 Absolutely. Okay, okay, okay.
[silence]

Marc: 01:07:59.476 The How to Get Help should probably not be a standalone step, but be split across where they-- every time they may get stuck, remind them how to get help. Like from the Set Up a Dev Environment until Runs a Project they may need to be reminded how to get help, basically.

Remi: 01:08:29.275 What I would suggest also, to humanize a little bit, maybe is to have very small motivational videos. We could write some scripts for each activity they do because they will spend a lot of time trying to do the activity. And just to have a motivation to do that, to see someone speaking like the teacher or one of us speaking and saying, "You will do that and we know it's difficult, but you will. We are pretty sure you will find help and you will be able to do that." Just a simple video, 30 seconds, one-minute maximum, to motivate everybody for each step. I don't know why I think that seeing the face of someone will be--

Marc: 01:09:37.979 Helpful.

Remi: 01:09:39.115 --helpful, but--

Marc: 01:09:41.458 No, it's true that a week with only activities will be a bit hard to follow if you don't have some different kinds of interactions/whatever. How do they call it? Different kinds of--

Remi: 01:09:59.725 Modalities. Pedagogical modality. So anyway, writing some very small motivational scripts that we could record and video looks okay for me, even if it is a bad video, just seeing people saying something, a few seconds would be-- I think anyway it's a good practice.

Xavier: 01:10:25.731 And don't forget also that we have interviews that we'll sprinkle through the course so we can have a few chosen answers saying, "Yeah, setting up the development is always the thing that I dread. It's always broken in our project," blah, blah, blah. So they get a bit of a context on-- yeah, that's normal to struggle a bit on that.
[silence]

Xavier: 01:11:19.223 So which part would be the project tips, by the way, because this week is kind of almost all activity based on their project trends. So do we keep the--?

Remi: 01:11:31.893 What do you mean by the project tips?

Xavier: 01:11:34.878 The project tips; that's the section we've put in at the end of every section where they do work on their project, but this week is kind of almost exclusive. So do we structure it differently?

Marc: 01:11:48.723 We could add a project tips at the end about what sort of bug to reproduce because if it's a bug they want to fix, they should choose it carefully and they should choose it in a way that will know how to fix it at home. Like what are the easy-- like if there is an easy fix tag, how to find bugs that are constantly reproducible and not intermittently reproducible. Things like that could also be tips at the end because we end with reproducible but not any bug is a good bug.

Xavier: 01:12:24.410 But then, should then not be something to say at the beginning so that they directly pick the right one?

Marc: 01:12:32.375 It's currently written as Reproduce A Bug, but we don't have any intro to that, so. We don't even explain how to [reproduct?] what they are expected to do in reproducible section. But it could replace--

Xavier: 01:12:50.823 Yeah, it's true that-- I see what you mean. Yeah, it's true that before it's, Build the Projects. Yeah. Maybe that could be that.

Remi: 01:13:04.201 It's after, no? First you have to build and the reproduce the bug.

Xavier: 01:13:09.737 Yeah. Yeah. Yeah. Yeah. Yeah. Definitely.

Remi: 01:13:13.970 So choosing the right bug is also complex.

Xavier: 01:13:24.638 Think it could actually be something on which Loic would have good insights. And I know he is busy lately so he hasn't been able to be around. But I'm almost tempted to try and to push him to take that section because that's what he was doing repetitively with many new contributors as a mentor for quite some time. So I think he has a lot of experience with that and he would probably be able to give good advice as well. I would be more-- I mean I can spot one when I see one, but explaining how to find one is a bit more tricky, I think. But anyway, in any case come back to that. That could be the project tip of the week. Yeah, that makes sense.

Remi: 01:14:20.872 Should they ask either the community of the project or someone if they chose a good bug?

Xavier: 01:14:36.019 Yes. Yeah. Actually, ideally they wouldn't even be the one picking it. It's true that that might be things to integrate in that way. I've kind of touched on that in the past but that would be important here. They might want to look at obvious places like the least of small bytes bugs, etc. But again, they will want to be using their established contacts and to try to get recommendation on that. Like if there are some people that have seen that they are active, right? That's a good moment to say, "Oh, by the way, I'm looking at-- at solving something really simple to get started, you know, and advice." And especially when you do it this way, you don't just get something that technically you can solve. You also get something that a main thing of the project is motivated to look at and merge, which can make a huge difference in terms of, "Is my bug going to get any attention?" So again, that's something we already touched on in the past with other things like documentation and etc. But we all need to rate [inaudible] that, yeah.
[silence]

Marc: 01:16:19.023 Okay. I think we have a good list of chapters.

Remi: 01:16:24.172 Yes.

Marc: 01:16:25.679 We now need to assign people.

Remi: 01:16:30.985 Let's put Olivier everywhere because he is not here. [laughter]

Xavier: 01:16:36.460 Yeah. And as a heads-up - I think I mentioned that in the past - since I've taken a lot of the stuff from the first modules, in terms of project tips and coordination, I'm going to be a lot lighter on this and the next module and the remaining two, just to be able to focus on that. I think I've got a good quota already.

Polyedre: 01:17:01.723 I can do the Set Up the Dev Environment thing. I can try at least.

Remi: 01:17:08.505 Okay.

Marc: 01:17:18.056 I will put Rémy in charge of the whole module because it's a module primarily about teaching technical stuff, and-- well, basically a teacher of technical stuff.

Xavier: 01:17:36.669 I am for that too. That's great.

Marc: 01:17:38.845 With a lot of basic errors people can make, so.

Remi: 01:17:49.099 Yes, yes, yes.

Marc: 01:17:50.371 If you don't want, then you can put me instead, then.

Remi: 01:17:52.912 No. I--

Xavier: 01:17:53.597 Yeah. I--

Remi: 01:17:54.460 --like module 2 and 5, so it's okay.
[silence]

Marc: 01:18:08.809 Okay. I will put Build the Project part.
[silence]

Marc: 01:18:25.992 I can also take the How To Get Help part if you want.

Xavier: 01:18:34.151 Sure.

Marc: 01:18:34.651 Okay. I will put myself. And, Rémy, are there specific steps that you want?

Remi: 01:18:44.166 Maybe I can help you with Build the Project, on Windows, especially.

Marc: 01:18:52.741 Sure.

Remi: 01:18:53.815 I have some advices, or.

Marc: 01:18:57.938 Yeah.
[silence]

Marc: 01:19:11.414 Let's put Loic here and here. I put Loic in Project Tips, Find a Good Bug, and Reproduce a Bug even if the Reproduce a Bug parts might be easier than finding a-- it's much harder to find a good bug than to reproduce it.

Xavier: 01:19:36.531 Yeah. Yeah. That's true. But he might also have some good insights on this because that's something he's really good at, so.

Marc: 01:19:50.502 Yeah.

Xavier: 01:19:57.277 And actually, just to have it in this transcript. One thing that I would recommend in the Reproduce a Bug is to do that as a test, like basically writing a test that reproduce the bug. Because then basically the bug is half-solved already, you just have to make the test pass and then that might be a good approach. And it might depend on the project, of course, but it might be good to recommend that.

Marc: 01:20:24.951 If they already have tests but they do not test literally everything then it's good. If they do not have tests, sometimes it can be a bit touchy to add a first test.

Xavier: 01:20:35.745 Yeah, that's for sure. But at the same time, a project without tests, I'm not sure with going there.

Marc: 01:20:43.434 Oh, I know. Lots of project without tests?

Xavier: 01:20:48.000 I know. But as a contributor, that has a level of difficulty because you don't know what you break before other people test it and stuff, so yeah.

Marc: 01:21:01.733 We haven't put in this module, like the Contribute a Typo Fix or Documentation Fix thing. Should we or is it long enough like that?

Xavier: 01:21:20.285 Well, I think it might end up being something to do in the previous weeks, potentially. I think I'm not completely clear on exactly the progression of contribution. I think there's a little bit of overlap and we have to discuss different things in different weeks that sometimes are not completely well-ordered. But in terms of typo fix on the documentation side, it tends to be the easiest. But it's true that there could also be a typo fix on the code. Like it can be fixing a typo in a comment or something like that. And these can be done earlier because you don't even really need the development environment set up for that because this typo on the comment is not going to break the build. That might be something that come in previous weeks.

Marc: 01:22:19.621 Yeah, but really strictly is the first time you really have an opportunity to find a typo in the documentation or in the instructions or in the code is when you actually have to do something like building it. So I think that--

Xavier: 01:22:36.721 Well--

Marc: 01:22:37.230 --the first interaction they have with the project's doc or the project's instructions or the project's code, and the first real opportunity they will organically find a problem and something worse than [inaudible].

Xavier: 01:22:54.720 Yeah, if you're thinking of organically, yes. But the thing is that that's something that we might want to enforce a little bit earlier because that will allow them to go through the process of contributing and following the same path as code contribution but without the technical element, which is what, kind of, arrives in this week. But generally, that's good to have seen that before because then if they have seen the whole process for contribution before that, when they arrive in this week they can really focus, okay, like, "I want to fix a little bug so I will focus on the technical part." I know that's one of the things Loic did almost at the very beginning, is to-- like even if you don't know the project, if you haven't built anything, just browse the documentation, the code for like-- find a typo somewhere, it doesn't matter, just to be able to do that process once.

Marc: 01:23:56.490 Yeah, I don't know. As a maintainer, if I see someone doing a match request for one typo in one file, I will just say, "Meh." Yeah.

Xavier: 01:24:08.066 How [inaudible]--?

Marc: 01:24:08.585 I would say, "Yes, there is typo, but there is also a typo two lines after," because it's probably the same person who doesn't write English that wrote this. And wrote, basically, this and a whole lot more comments. And I would say, "Yeah." Not sure I see the point of fixing one typo.

Xavier: 01:24:29.181 Well, but then that's actually where we can also provide an advice to the maintainer, because there is a specific reason why we do that. It's to help facilitating the onboarding of the newcomer. Obviously, it would be all maybe something to explain also to have like a little bit of pedagogy on why we do that. It's just like a-- just [crosstalk] follow the methodology.

Marc: 01:24:51.283 I mean, it's not harmless. If I take some time to merge a typo, it means that the CI will run for two hours. And during those two hours, I have to rebase all the merge request that they want to merge after that. And the CI will run for two more hours on all merge requests that are pending. It can be more work for the maintainer to match several merge requests that do almost nothing.

Xavier: 01:25:28.689 Okay. That might depend on the project, then.

Marc: 01:25:33.117 It depends on the project, of course.

Xavier: 01:25:35.948 Yeah, it's true that in that case, it's a little bit of a bother, for sure. But that must come to bite you a few times, actually, to have to do that every time, but yeah. It's true. I mean, at the same time--

Marc: 01:25:55.980 It does most of the non-code translations, non-trivial-- most of the non-code contributions are translations, and usually it's like, 1,000 more translation at the time. And so I imagine that it's definitely worth it, and even if I-- if a translator was suggesting I add one line, and then I do another merge request for another line, I will say, "No, no, no. You finish that file, and then I will consider it."

Xavier: 01:26:26.935 Okay. I see. I mean, that might-- in that case, that might be good feedback, is that if-- there might still be, I think, an incentive for the maintainer to want to merge that because they want the new contributor, so that also helps. But even if, in your case, you're like, "Okay, I need something a bit more substantial before merging it," then that can just be an incentive. "Okay, I will go through all the files, and fix the spellings everywhere." And then maybe that becomes enough for you to, I don't know. And I don't know if, in that case, that makes a difference. But that can also be something that is more related to documentation, because then that might not be the whole build process. But they will still have construed a contribution process, at least once.

Marc: 01:27:21.423 Yeah. I would probably not reject someone saying, "Oh, I've fixed this typo." But I will say, "This typo is still present at 20 other places, and so maybe--"

Xavier: 01:27:34.613 Yep. Then, fair enough. That also teaches what you as a maintainer expect, what's your preference. Then it's good for them because they know, "Oh, okay. Every time I submit a pull request, actually Marc has to do two hours of rebasing and stuff. So let's try to make it worth his while next time."

Marc: 01:27:55.638 I mean, I don't, but the CI has to, and so I have to wait basically.

Xavier: 01:28:00.207 Yeah. Yeah, yeah.

Remi: 01:28:09.438 And so where do we put this?

Xavier: 01:28:13.616 Which part? The--

Remi: 01:28:15.458 [crosstalk].

Xavier: 01:28:16.636 The typo? Yeah. I think there might be something from the weeks before. I have my list that I need to kind of have a-- once we have gone through everything to have a hard look at what we put in different project tips because I think there are some things that needs to shift around.

Marc: 01:28:34.001 It's actually written in the project tips for module 2.

Xavier: 01:28:38.390 Okay. Yeah.

Marc: 01:28:40.233 But it's with a question mark.

Xavier: 01:28:43.761 Yeah. Yeah, that's why-- and actually, they have cued things like that because I mean, it makes sense for you. We've been brainstorming so we have not always been very structured in things, but those need some ordering. I think week two might be a good one because it's true that we are talking about Git merges and etc., so it might make sense.

Marc: 01:29:04.988 Yes. And we went through the whole process of merge requests stuff, so.

Remi: 01:29:10.603 Yes.

Xavier: 01:29:14.253 All right. So I will have to drop soon because I have a meeting afterwards, but--

Marc: 01:29:20.512 I think we're good for this week.

Xavier: 01:29:23.450 All right. Perfect. Oh, before we stop, do we have a schedule for the business meeting? I know that's like the returning topic of the time for a long time, but I don't think so. We have one with the edX but I don't think we have a--

Remi: 01:29:50.062 No? Should we plan that right now?

Xavier: 01:29:54.420 Sure. I mean I think last time we talked about the fact that it would depend on the availability of people on the business sides.

Marc: 01:30:04.782 Rémy, you wanted to do something written before, right?

Remi: 01:30:07.966 Yes. I mean, all the ideas you have, Xavier, did you write them or should we take 30 minutes together to write everything down?

Xavier: 01:30:27.645 I mean we can write them but wouldn't it be good to talk first because that's my ideas, but I'm sure there is a very different perspective on your school's side.

Remi: 01:30:39.933 I understand. They want to prepare if-- they want to have ideas of how you want to make money or other contributors. Think about it. So let's send them a written something, one-pager, and once they've thought a lot then we can meet then. Their way of processing things is very [crosstalk].

Xavier: 01:31:12.109 I mean, don't mind that but it's just that sometimes to write that first paper it's good to have at least met the person so that you know a bit about they think. So, I mean, we can skip that step but that just makes it a bit harder to prepare something they will like.

Remi: 01:31:26.225 I understand. Okay.

Xavier: 01:31:30.939 But you tell me you know them better so you know what will work better with them, but.

Remi: 01:31:36.651 I know for sure it will work better if we send them a few bullet points.

Xavier: 01:31:42.942 Okay. So you--

Remi: 01:31:43.721 If there's like 10 bullet points and--

Marc: 01:31:46.969 Because they will have absolutely no idea what to expect from a meeting if they don't have anything before because they don't really know the project or the context of the free software environment or what already exists in the world.

Xavier: 01:32:03.280 Okay. Well, then maybe we can do just like a kind of a brainstorming thing between us on what to write because you also know them better, so I think that will already work better than if I tried to write something out of the box. I'll just try to prepare some points but I think I have most of them in mind and most of them have already been put into some tickets of some of the brainstorming things. But yeah, if we can do already that preliminary meeting, I think that would be helpful. Just the three of us or whoever else wants to participate actually, because is--

Remi: 01:32:39.956 So Marc, I went to your Calendly. 30 minutes should be okay?

Xavier: 01:32:50.612 That works for me.

Remi: 01:32:52.753 Marc, can you do, let me see, tomorrow? Or maybe not tomorrow.

Marc: 01:33:06.023 I don't have anything this week except for Friday because Thursday is non-working but I will work anyway. And Friday is school closed but I will work anyway because I have meetings. But I don't have anything for tomorrow and Wednesday.

Remi: 01:33:24.240 For tomorrow 6:00?

Marc: 01:33:27.701 What do you mean tomorrow 6:00? Tomorrow is 11th.

Remi: 01:33:29.843 Tomorrow at 6:00 PM.

Marc: 01:33:31.767 I don't know what time it will be for Xavier, but it may be very early, 6:00--

Xavier: 01:33:45.125 No, that's fine. I think you got it from my Calendly. That's my remaining spot for tomorrow. Yeah, that's fine.

Remi: 01:33:52.236 Or the week after, same time. Or even 7:30.

Xavier: 01:34:02.343 The week after, I'll be back in Europe. But tomorrow would work perfect for me. If you're good, I really don't mind. 6:00 AM, it's my normal time to start the meetings, so.

Remi: 01:34:19.005 Marc?

Marc: 01:34:20.620 Yeah, 6:00 PM, I'm awake at 6:00 PM. I'm not awake at 6:00 AM but I'm awake at--

Xavier: 01:34:27.683 Yeah. Well, that the other thing is that then I'm done with my meeting for the day, so I kind of like that.

Marc: 01:34:33.942 So you can work?

Xavier: 01:34:35.409 Yeah, pretty much.

Remi: 01:34:38.821 All right. I'll send you an invite then.

Marc: 01:34:42.503 We should make an issue and a note in the chat so that people know about it.

Xavier: 01:34:50.927 Yep. I can take care of that.

Marc: 01:34:53.912 Cool. Yeah, you can say that we are sorry for the short notice but it's been discussed for some time and it's easier tomorrow.

Xavier: 01:35:06.775 Yeah, actually, I'm wondering if we even have a right ticket for this business model.

Remi: 01:35:14.286 Yes. Yes. Yes. Yes, there is one.

Xavier: 01:35:17.025 Yeah. Actually, I'll put it in that ticket because then it doesn't come from nowhere.

Remi: 01:35:26.618 Number 13.
[silence]

Remi: 01:35:45.899 All right. So see you tomorrow, Xavier?

Xavier: 01:35:48.763 Yep. Perfect. That sounds good. All right. Have a good day.

Remi: 01:35:54.221 You too. Thanks Polliard.
