# Brainstorm - Module 4 (Meeting Transcript)

Video: https://digimedia1.r2.enst.fr/playback/presentation/2.0/playback.html?meetingId=73d3cd53ec8ede3f636b9a782ecc9f6375cf2271-1614015027921

Marc: 00:00:01.929 Okay. Welcome back, everyone, to this new session about the MOOC contributing to FLOSS. This week will be dedicated to the contributing part, which means the human interactions within a project, and basically, how people interact with each other within a project and things that are linked to these interactions. So in particular, where they interact, IRC, mailing lists, Slack/Mattermost/Rocket.Chat/Matrix/Zulip/others. Mailing lists, the [inaudible] relationship within a project, code of conduct, diversity issues, etc., etc.

Marc: 00:01:03.379 So maybe the first question that we could try to answer is what are the points we want to convey in this week? I think that's something important first to not get lost in lots of possible discussions and topics of interest. So, for instance, the big part about what the students will need to do relatively to their project is get in touch with the project they want to interact with. So when I receive the project [is on IRC?] and find out about the code and the bug trackers and where things are in the projects, basically, where they can interact with the project.

Loïc: 00:02:02.604 What I find most important--

Rémi: 00:02:04.859 Should we introduce ourselves before, or?

Marc: 00:02:08.942 Oh, we could introduce ourselves, but we've all been there before, so. Yeah. As you wish, but I think if people watched the previous videos, they will know who we are.

Rémi: 00:02:22.413 Yes. [crosstalk].

Marc: 00:02:23.540 And we know who we are because we were all at the same time last week.

Loïc: 00:02:28.819 So from my point of view, this week should be about conveying the idea, which is unusual, that the majority of the effort regarding contribution is social interaction in nature and not technical and that it is normal to spend a lot of time dealing with human interactions, rather than bugs, hardcore stuff.

Marc: 00:03:06.554 Let me load the [inaudible] so we can take notes.
[silence]

Rémi: 00:03:24.485 So not only get in touch, but also get in touch is too light. Sounds a little bit too light. Maybe it's a light objective for the student of the MOOC. But if you really want to contribute to a code base, you would have to be involved more deeply with the community.

Xavier: 00:03:53.336 I think even for a single contribution. Often the mistake that a lot of people make is to think they just open a pull request, send it, and then they are done. And they forget all the part that comes before that, which is social, like you're trying to figure out how the project works, what people's motivation, and etc. And the part after, which is still social, which is addressing comments from the reviewers and etc., there is a technical aspect to that, but there is a social aspect that's quite important because you need to understand what the reviewer really cares about, what do they want, how quickly you need to interact with them, and etc. So, yeah. It's of course even more important if you plan to be a long-term contributor. But even for a single shot, I think that social aspect is pretty central.

Loïc: 00:04:56.921 And also about getting in touch. I always begin with the idea that you should get in touch immediately. And I gave examples of how to get in touch immediately. And as soon as I give the examples I ask them to do that, and this is essentially ask a question, go to find the IRC channel or whatever channel, ask a question. And [God?] this is weird because you don't know what to ask and maybe it's artificial. So you have to figure out how to mingle. It's like being in a party for the first time with lots of people you don't know. You go to someone, you have a glass in your hand, and you engage the conversation. And you don't need to be brilliant, but you need to do something that is appealing to the person you talk to.

Xavier: 00:06:07.954 Yeah. And that is often I find psychologically the hardest type, and I mean especially I guess I mean in geek communities where the social thing is not necessarily always everyone's favorite part, and I mean I'm also going to make a point I've made a few times already. I think we see that in the structure of these calls currently. We wait until week four to initiate the contact. And, again, I would do that way, way earlier because that's like the biggest blocker. But [crosstalk] about that.

Loïc: 00:06:48.499 Again you have to explain. You also have to explain why it's important to do it immediately. What's the upside? How would you explain it?

Xavier: 00:07:00.992 Well, I think your parallel with the party is a pretty good one. The more you wait in your corner part of the evening the more you're kind of building the difficulty to do that. The earlier you do it the more you have maybe like a very superficial conversation at the beginning, but you'll get to more and more interesting things earlier. So I don't know if that's the right explanation, but I think that's definitely part of the mechanism.

Loïc: 00:07:30.465 That's a good one. Yeah. Yeah. There is also the fact that someone will accept your contribution. So someone will actually pay attention to what you give them, and you need that. And it's more likely to happen with people you know for longer rather than shorter. The probability that someone you've known even slightly for let's say four weeks accepting your contribution is higher than someone you met yesterday because you've been around for four weeks. So when the time comes to accept your contribution, you're already part of the team somehow, a little bit. Where if you wait longer then you don't benefit from that. So there is the growing, the more interesting conversation. There is being part of the team over time. So you don't need to be brilliant. You need to be around for longer rather than not.

Xavier: 00:08:46.806 And there's the part about other people knowing you, but also I think we are social animals, so we develop our understanding of the group of the projects through our own interactions. So by having put ourselves out there, by having interacted, probably even the contribution will be different than the one where we would have waited.

Loïc: 00:09:10.952 Oh, there is also one other benefit. Something we see quite a lot when professionals-- someone who worked in IT for 10 years is very skilled in his field or her field and contributes to a free software that is in her or his field then of course they are very proud to bring their expertise to this project because they are experts. And often times they assume, sometimes wrongly, that the people writing free software are amateurs, that they're not professionals, they are not really paid for it. It's kind of a hobby for them. So they don't take them really very seriously. But by interacting, they're not just looking from a distance, from a higher point of view, looking down to people. When they interact, there will be pushback. There will be time where they make mistakes. So it brings them closer. So it helps the experts to have more friendly interactions with people who are less skilled, and I've seen that quite a lot.

Marc: 00:10:37.258 And there's also the opposite case where you assume that everyone around must be an expert in whatever. And actually, they're not, and your code is not maintainable because it's much too complicated, and it does stuff that is not in the scope even if it could be in scope if experts were around more. And you submit your like 20,000 lines patch, and we cannot maintain that because we don't understand any of what it does.

Loïc: 00:11:10.750 I don't know if you envision including this example into the course, but there is this great example of-- in the context of OpenStack, Hewlett-Packard, forked the code base and created a huge gift, a huge contribution, and when it was time to be merged, it was a catastrophe of epic proportions to the point that management acknowledged it etc. So it made the news. So it's a good example of, okay, don't wait too long. They waited a year or two years, I don't remember, and that was way too much.

Marc: 00:11:59.736 That's because OpenStack moves fast.

Loïc: 00:12:03.154 But two years, more than a year, that's a lot.

Rémi: 00:12:07.853 I think OpenStack is more of a model that is an open consortium of a lot of parties, rather than an open community. I don't know if there is difference. A consortium is like a lot of different companies contributing to the code base and a community. And an open community is you don't have any big company or firm that controls the source code. I don't know if there is a distinction between the two type of communities. I mean, it depends if the project is driven by a firm, a company, or driven by a community first.

Loïc: 00:13:03.019 Sure, yeah. It makes a big difference indeed. And also it segregates the time at which you can make the interactions. So it's another example I gave about asserting who you talk to. If you deal with people who contribute in their spare time, it will be in the evening, it will be during the weekends. And if it's paid staff, they won't be around except during working hours.

Xavier: 00:13:42.462 And the working hours can depend on time zones too, so it's another criteria actually, location I guess.

Loïc: 00:13:57.638 A big question for me and something that is really difficult, is how to convey the idea that it's worth the effort. And this is not something I've succeeded to do very well.

Marc: 00:14:14.342 Is it?

Loïc: 00:14:16.297 Is it? Yeah. Of course it is, of course. Yeah. But it's difficult because in the end, it's also the mentoring part. The real contribution showed me about every time that I failed to convey that in theory. And it's only when faced with the real need for interaction, that it becomes an issue that is addressed, and that people start to understand that oh, that's really a thing. It's something I experienced first hand. So I don't know if it's possible to convey that just by talking. People need to have-- yeah, go ahead?

Marc: 00:15:14.042 Ideally they would be convinced by the weeks before.

Xavier: 00:15:21.247 But wait, is your argument whether it's worth contributing or worth having the social interaction to contribute?

Loïc: 00:15:33.446 You're asking me?

Xavier: 00:15:34.505 Yeah.

Loïc: 00:15:36.759 No, well my point is how to convey that in theory.

Xavier: 00:15:43.407 But how to convey that it's important to have a social interaction to contribute, or that it--?

Loïc: 00:15:49.284 Yes.

Xavier: 00:15:49.790 Okay. For that, I think maybe something that more and more people realize even in the computer industries is that networking is an important aspect. Who you know, will allow you to get jobs in the future, and it's kind of a related topic here because you need people to know you, to trust you, and to allow you to take responsibility in things including doing a contribution.

Loïc: 00:16:22.505 That's a very good, yeah. I never used that. Very good. Yeah. Because in the end, so what you can say is, you can make the parallel when you look for a job you ask someone to give you a job. And for that you need a network of people you trust and people recommend you, and then you get the job. Now if you translate that into contributing, you need someone to take your contribution. Now there is a bit of explaining to say, because some people say, "Well I give the contribution to them, so of course, they will accept it." But of course not, because you have to convince them it's worth accepting. And then you have the networking part that works like a job.

Xavier: 00:17:14.565 Yeah. Especially because I think that's probably something that is often overlooked that might be worth explaining is that when we do a contribution, sure, there is the time of the person doing the contribution that is given but there is a huge gift, several huge gifts on the others side, from the upstream side. First the maintenance, it means that whatever patch you had before you had to carry over in each release and etc. Now somebody else is taking care of that, and that's really not small. And there is also the time that is spent reviewing, which is like, I take that from you actually, that's a huge non-gift that you have. People with a lot of skills will spend time for free correcting your code, giving you advices and etc. And that's actually, I'm not clear who is giving the most actually in a lot of cases, so.

Loïc: 00:18:08.786 Yeah. It depends. Yeah.
[silence]

Rémi: 00:18:22.693 I was also wondering if the organization of the community is something relevant for big project, even more, if it is driven by a company, because sometimes you have core developers. You have, I don't know plug-in developers. And you have other type of community. There is a kind of organization that has to be explained to know which part of the organization to contact first. And also there is a ramp up meaning that when you join a community there is also this-- it takes time to be heard.

Loïc: 00:19:19.981 Could you give an example? I see what you mean, but.

Rémi: 00:19:27.612 Well, I'm thinking about a core developer team. Even sometimes the core developer, they communicate with each other outside of the public community of the project. They have private channels, somehow. Because I think that, for some projects, to maintain the integrity of the project, you have to have what is called a core-developer team that communicates a lot. Otherwise, the integrity of the project will be blown up by many people. And so this core-developer team is communicating privately almost. And sometimes you have like onions. You have the core developer.

Loïc: 00:20:33.373 Yeah, it's a very good point. If I had to rephrase it - and please stop me if I'm wrong - it's making the difference between the places where you can communicate with people because you can just hear them, you're in the same place, and other places, or other parts of the project where it's behind closed doors for whatever reason. You mentioned a core team. It could be like Google on Android. They develop behind closed doors within Google and then release once a year. So there are a variety of reasons why you don't hear part of the conversations, and you have to be able to detect that. Otherwise, you talk to walls and it can be very frustrating.

Marc: 00:21:26.056 Can also be due to lack of documentations about the proper discussion channels.

Loïc: 00:21:31.894 Yeah. Also maybe you didn't find it.

Xavier: 00:21:34.973 Yeah. I mean, it's true that the places where things are discussed even in a fully public project can be something intricate to map and to figure out just like we were talking about like the models and etc. Because you might have, I don't know that the issue tracker on GitLab, but there might be a forum besides that, the IRC channel. And there might be some like weekly meetings, like the ones we are having here, that happens where a lot of discussions happen. But until you discover that they exist, you are completely out of those discussions. And it's true that identifying places is important. I would say also identifying roles, like what you were mentioning, Remy, about core developers is important because knowing who you're talking to is important because sometimes the most chatty people on IRC or whatever will be the complete noob that the project has learned to ignore because he's the one doing a lot of noise, but not doing much. But if you just follow blindly what that person tells you, you just get nowhere. So that might be also important.
[silence]

Loïc: 00:22:55.682 Yeah. So an exercise to help with that would be to take a project and then the homepage of a project and say, "Okay, find the URLs that describe the communication channels."

Rémi: 00:23:16.775 And also, I was thinking that there is a kind of a different phase. Yeah, different phases when you want to contribute code to a project. The first is the learning phase where you have to learn everything, how it works, where to find the bugs. So you have to communicate doing that. And the second phase is also you have fixed the bug, but you still have to coordinate with--

Marc: 00:23:57.760 We lost you, Remy. We lost you at coordinate.

Rémi: 00:24:04.046 All right. It's coming back. So yeah, sometimes I have a blackout of 15 seconds on my audio. I don't know why. So you have to coordinate to integrate your bug fix. And again, you have to communicate with the community like two phases to learn and to put your a bug fix in the code or to make it approved or something. And it's a different kind of communication.

Marc: 00:24:38.384 Really different one?

Xavier: 00:24:42.433 Yeah. If I understood what you said correctly is that if you want to do a plugin, then you can just almost publish it in whatever plugins repository without much of communication. But if you want that to be included in the core then you will have a lot more scrutiny because people are going to be like, "Oh, we'll have to have to maintain that." Like in Open edX there is a measure of that, where everything was going to the core originally and at some point, the kind of the core maintainers the [inaudible] etc. realized that, "Oh, that's a lot of maintenance to house. Let's push some stuff to plugins." So definitely that mechanism.

Loïc: 00:25:25.808 I have a slide in Upstream University where there are if I remember correctly five levels. It goes to what you're saying. When you begin, the first contribution I think I mentioned is answering a question on the mailing list or IRC from someone who asks it. It's a contribution. It's not code, etc. but you help. And then second one would be fixing a typo in the documentation. Third one would be fixing a easy hack bug in a very, very trivial way. And then, you level up until the last level would be reorganizing the governance of the project. And the level of communication you need for that is completely different from the level of communication you need to answer someone on IRC.

Xavier: 00:26:32.869 Yeah, I like that. Governance is the ultimate stage of contribution. It's quite true, actually, yep.

Loïc: 00:26:39.420 True. Yeah. Then you take over, essentially.

Xavier: 00:26:43.839 Well, no. Because if you contributed, you'd do it with other people. But it's true that you touched [inaudible] on there, yeah.
[silence]

Loïc: 00:27:04.627 I see there is something about code of conducts.

Marc: 00:27:12.698 Yep. Because they are basically the rules for people interacting in the spaces of the project.

S?: 00:27:25.059 [crosstalk] right?

Marc: 00:27:27.548 Yep. And I think it's worth mentioning that they exist and that you should not completely-- you should not overlook them. It's like people did put them in there for a reason and you have to, basically, respect that and follow them. Especially since it's usually behave like a human being with the others and just respect others.

Loïc: 00:28:00.793 Actually, Upstream University is dated about five to eight years ago, which is the time where a code of conduct became a thing. So I actually don't remember what we talked about regarding the rule of engagements, or whatever they were called at the time. In the past five years, I have been involved in a lot of communities where code of conducts were developed, abused, ignored. And so, it's a very, very interesting topic, and there are a lot of stories to tell. And it's also quite new.

Loïc: 00:28:55.887 So I figure, this is the part of the week that people will have the less problem paying attention to because it's like an adventure. It started with Jacob [inaudible] scandal before even the Me Too, and you have a lot to tell about that. And then, you can go to concrete examples of the project. And also, for me, in this area, there is one thing that is absolutely essential to convey, that is not conveyed anywhere. It's that a code of conduct is the last resort when everything went to shit. And so, a lot of people assume that if you add a code of conduct in a community, then everything will be fine. But it's not true.

Loïc: 00:30:01.836 What actually works is what the LibreOffice community does, which is a shining example of they have a code of conduct, but they almost never use it. And the reason for that is because they mediate in between. That is when you arrive in a community it will be healthy, not because they have a code of conduct, but because they have people who pay attention and who spend actual time defusing situations where people start to be tense towards each other. And that actually works. So this difference between mediation and enforcing a code of conduct is something that makes it infinitely easier to engage with a community.
[silence]

Xavier: 00:31:06.333 But that's also from the point of view of the community organizers because from the point of view of the contributor, it's still useful to read the reference of what that community conduct should be and etc. I mean, they are often worded in a way that is very idealistic. So I don't know if it's always something you can follow to the letter. It's rarely like practical advice I think. It's more like how we should all be in an ideal world. But it is still a good starting point and having at least read it or glossed over it is very important.

Loïc: 00:31:47.616 No. I'm not saying it should be ignored. It's super important to have one. And communities where there is no code of conduct that's a really bad sign. In 2021 it's a really negative sign. So if you don't have a code of conduct, it's likely that the community will be dysfunctional in a way. Now from your contractual code of conduct--

Marc: 00:32:12.120 Or small--

Loïc: 00:32:14.574 Sorry, what?

Marc: 00:32:15.224 Or small community because small communities tend to basically work in small discussions. It's self-mediated in essence, usually.

Loïc: 00:32:34.363 Yes, but even with a few people nowadays it's really, really frequent, especially if a project just started, they will say, "Okay, well, what do we have? We have a license. We have a code of conduct. We have a repository." So it became a reflex almost. But you're right, if the community is small it may be because-- but from the point of view of the contributor, once you checked that you have this code of conduct, the second thing you should check, and that is much more important to you is, is there someone who is going to smooth things for you as a newcomer. Because if there is someone abusive from time to time, not enough to get kicked out, you will suffer from it mildly, but it will not be a pleasant experience. Where if you have someone who pays attention to that and makes sure that people who are a little bit hostile or aggressive do not bother newcomers, it makes a huge difference. I have a few examples. So that's the tricky part of the course. It's great in the course to give examples. When you say something, you say, "Okay. I will give you an example." But in this case, it's almost impossible to give an example that is an actual one without pointing fingers. And that's not something you want to do in a course.

Marc: 00:34:18.170 Yeah. Definitely. We could give the most famous examples, which are for instance Linus Torvalds. This one people could know, and it's not a big finger pointed because he admitted that it was a problem and he changed it.

Loïc: 00:34:44.458 Yes. And the fact that he admitted the problem, makes it a good example. You're right. Which was not the case a few years ago where people would reply, "Okay, he behaves badly, but apparently it's fine. Maybe it's part of the culture. Maybe it's what free software is about, being abused by powerful people."

Xavier: 00:35:11.789 And I mean, I think you can take examples. You can always change the names to make it a bit anonymous and etc. But there's an interest in having kind of a diversity of point of view because if the first time-- because we can't point fingers, the first interaction or first experience that people will have will be when they are actually confronted to that. Then that's worse. We'd have missed a bit of our role of trying to prepare them to what they can be. And sometime that can be quite nasty, and think it might be good to show that.

Loïc: 00:35:52.526 So I will shortly give you an example. You tell me if that flies. I don't think it does, but. So two years ago I was mentoring for a GSoC project in a free-software project. And we were two mentors. And we were tasked to select proposals. And in GSoC, there are newcomers. They come to the project hoping to be selected to work on the project and be paid for it. So in many ways, there are people like the participants of the course.

Loïc: 00:36:30.689 And what happened, and what I was shocked to discover, is that my co-mentor was really not himself when dealing with these candidates. He would talk to them briskly. He was not insulting them. But the person I was used to interact with, was much nicer, polite, attentive. And when dealing with these newcomers, frankly speaking, he was an asshole. And I didn't feel like telling him that. And it was at a time - it was two years ago - and the project he was involved in had a code of conduct and was super strict about it, and they never did anything about that. And people saw the interactions. And I never talked to them about it.

Loïc: 00:37:30.590 So in effect, there was a code of conduct. There was a strong will to enforce it. This person, during a month, not always, but during a month, was abusive to at least five people in a very distinctive way and nothing happened. And what was missing-- in my point of view, what was missing is, no one was responsible for diffusing tensions because that was not the case where you would apply the code of conduct. He was not completely out of line, but he was, indeed, making people feel uncomfortable. That was plain and obvious, and that was missing. This example, I could have made that up completely. So does that help or not? I don't know.

Marc: 00:38:30.906 We could have--

Xavier: 00:38:32.176 I mean, it-- sorry, Mark.

Marc: 00:38:34.529 We could have this kind of generic examples because we are not pointing fingers. We are saying that these types of things happen, and they have to be watched for and called out, basically, and also that a code of conduct without proper enforcement is not much. And yeah. Go ahead, [inaudible].

Xavier: 00:39:08.690 Yeah. I think those examples are good, and we could probably get some from even the participants and etc. That's something they could probably enrich progressively. But I think it is useful because it shows that as a new contributor, which was the case of those GSoC members, you're like an easy prey for people who want to boost a bit their ego, and it might be good to recognize it. Because especially being new, again, a lot of cases you want to fit in, so you don't know what's exactly admissible or not. You don't want to be the one arriving and being like, "What is this behavior?" and being a problematic person. So there is a lot of, I think, pressure to just comply and go with it. And giving example where this was not okay might also push people to speak up a little bit more. And when that happens, just be prepared for it.

Loïc: 00:40:14.863 I wouldn't push for the-- during the course, I wouldn't push for the speaker part mainly because I didn't speak up in so many occasions even though I had seniority and whatnot. So it's one of the most difficult things to do.

Marc: 00:40:31.236 I don't think it's for the newcomers to speak up, basically. It's for the people in the project, and it's for the newcomers to recognize problematic situations. And don't come in the project if it's problematic. Like choose another project.

Xavier: 00:40:47.732 Yeah, it might be a more appropriate reaction for sure. Because we don't probably want to encourage the opposite behavior, which is someone who just likes to attract attention by just complaining about things immediately and etc., which is also a problem case, but.

Marc: 00:41:08.569 Yeah. Entering a project by complaining about already existing members of the project is not a good start. It's a recipe for your merge request would probably be delayed for reasons you can't understand. Because if people don't really like you, they may take longer looking at your merge request.

Loïc: 00:41:35.478 Which brings us to another point which is social. And it's super simple but never stressed enough is you obey the reviews that you get, not because you agree with them most of the time. But if it does not go against your contribution. And maybe you would have done things differently, but it does not break your contribution, then you will agree to everything. So essentially, when you're a newcomer, you submit yourself to the reviewer to the extent that your work is not degraded beyond recognition. And this is a social interaction being some people have trouble just doing what they're told for no reason like fixing the style of a function. It's a matter of style, and they may think it's fine. But telling them that in this case, when you're a new contributor, if you accept the maximum of reviews, then you will score points every time.

Xavier: 00:43:05.427 Yeah, I think it's especially hard, I think, in some areas where people, I mean, I might include myself in that, always want to know why and have a good reasons for things to happen. I was even trying to find a parallel thinking, "Oh, we could explain that through the fact that yeah, it's like a job. When you work, you come, your boss asked you to do something. You might not completely agree, but you will do it." But actually, even that one nowadays, I think he's a bit dangerous because a lot of people will say, "But no, at work, I will not accept to do something that I'm not asked, that I don't understand, that I don't approve, and etc."

Xavier: 00:43:50.959 And I think maybe a better parallel might be, again, your party like is that you come to a party, to a friend's party. If the host asks you to clean the dishes or to set the table in a specific way, you're not going to be like, "Why?" Sorry, like, you have to explain that, "But my preferred way of doing this is that", you're at someone's place, so you will conform to their way of doing things. And maybe if you're like a regular guest for a year, maybe then you start to say, "Hey, the way we set the table, maybe there is a better way," but that's because you know the host pretty well.

Loïc: 00:44:30.295 And that's where there is that thin line between complying to the demands of your host and being abused by the host who asks you to clean the toilets.

Xavier: 00:44:43.313 Yes, that was actually the second part I was noting from before about speaking up is that I think as long as you are within the limit of something that's acceptable for like, the contribution you feel welcome, it's party, it's nice, and etc., you will do things even if that's not your way of doing things. If at some point we feel pretty awkward, and you feel the host is abusive, and you're about to leave, I think that is however a good moment to speak up. Not like to enter a rage and start to break everything, but more say, "Okay, I'm leaving the party because I feel unwelcome and here are the reasons." That might be a good thing because often communities want to have new developers, new contributors, and etc. And if there is one who remain polite, who always complied a lot and at the end says, "Sorry, I can't stay because of that," that that is also a form of contribution in a way to the community.

Loïc: 00:45:43.176 It's excellent point. It's also super difficult. We had this conversation a lot in the past, you and me. And you know that I feel uncomfortable doing that. So I did it a few times, but very, very rarely. And yeah, but I agree that there is value in that if you're comfortable with taking this position when you leave essentially. I think that would-- Wikidata. I did what you suggested but only once with Wikidata.

Xavier: 00:46:17.674 Yeah, but it's difficult because that's a moment where you don't have much to gain, and it's a moment where you've already decided to leave. So the value that you give in that community has decreased. And often, the feedback, I'd like to say feedback is something that people have to deserve, right? If you don't care about someone-- say that for example, they're reviewing Airbnb places. A place I don't care about, I will not review it. It's only if this became a bit more personal that I will give a review. And giving advice or feedback to a coworker or to a boss or to someone like that, the people you don't care about, who you don't like, I think at the end you're generally not going to bother.

Loïc: 00:47:08.309 Yeah. It's a kind of last contribution. So your ultimate contribution when you leave is to say why.

Marc: 00:47:22.535 The lack of time in the best case.

Loïc: 00:47:29.709 Yeah. Of course.

Rémi: 00:47:30.451 Also, I researched through research papers a little bit very quickly, and I saw that there are methodologies to evaluate the strength and weaknesses of FLOSS communities with a systematic method to know if the project is mature or not and if the retention of newcomers is great or not and if the turnover is great or not. I mean, they have some metrics to evaluate some communities. And I was thinking what kind of activity do we do there as a student? What should we do? More specifically than get in touch. It doesn't mean anything.

Rémi: 00:48:31.580 And should we ask them to find some kind of metadata or this kind of metrics? Could they evaluate the community? And also, I was thinking about the second thing, the timeline. So you mentioned a lot of synchronous way of communicating like IRC. And I was thinking about how a student in the MOOC will synchronize the time that is dedicated to the MOOC. And the timeline of the FLOSS community which would be completely different and completely asynchronous even sometimes.

Loïc: 00:49:24.273 It's a topic to talk. Yeah. How can you meet the people? You have to be there in time for synchronous communication like IRC, but most of the communication are asynchronous. So you don't need to be-- if you come back every other day, that's probably fine. And other people also have asynchronous way of communicating. Maybe it's a good point to say that the pace at which the community evolves, you work, others work, it's something that you should measure, see if it matches. We mentioned time zones and working hours. That's part of it. Yeah. Good point.

Marc: 00:50:17.273 And maybe a small explainer on how different methods of communication work. Like don't open an IRC channel, ask is anyone around, and leave after one minute. Don't do that.

Rémi: 00:50:38.347 But more specifically, as an activity. So, okay, find the code of conduct with maybe something more concrete if there is one--

Marc: 00:50:47.625 We could make an IRC channel for the MOOC students, and just ask them to connect to that and post. Just for the sake of having used IRC.

Xavier: 00:51:05.254 Yeah, IRC. Or metrics, or yeah.

Marc: 00:51:07.398 Yeah. Yeah. But I mean, I mentioned IRC because there is no need for an account.

Loïc: 00:51:14.685 Or they could go to the synchronous channel of the project that they want to contribute to, take a screenshot of the question they asked and an answer they got. That's usually what I ask. Ask a question, get an answer, that's the exercise, do that.

Rémi: 00:51:33.782 So it should be a question that the answer could be given quickly.

Loïc: 00:51:42.620 Exactly.

Rémi: 00:51:42.587 Something very simple.

Loïc: 00:51:44.932 Yeah. And it could be just saying hi. Sometimes that works, but the goal is just that. And hi, has value if people say to that.

Marc: 00:51:58.049 Well, "Hi, I like your project, and I use it every day." Or something like that.

Xavier: 00:52:03.968 Yeah, that's actually something that we talked about in a previous week. Like that one of the exercise for social interaction could be to come and actually give some prize to the developers because we were worried, okay if we have 10,000 people all going to the project including questions it's not going to be nice, but on the other side if it's 10,000 people coming-- maybe not 10,000, but if it's a lot of people coming and saying we love the project, we probably wouldn't have too many complaints about that.

Loïc: 00:52:38.411 Probably not.

Xavier: 00:52:42.826 By the way, the stuff you pasted in the chat, Remy, are interesting. Made me realize, do we have a log saved of the chat for each of the sessions because there was often a lot of interesting links there right?

Rémi: 00:52:59.803 Yes.

Marc: 00:53:00.931 So recordings have [inaudible] have a chat within them. In the form of text, so you can click links, etc., etc. All right.

Rémi: 00:53:10.533 We have [inaudible] and it works perfectly. I had the same question with Mark last time. He gave me a demo of you have the text while you watch the video, and you can just copy paste. Yeah indeed. On Google Scholar when I put FLOSS communities as key words, it gives me quite interesting papers I have to say, so maybe we could have also some ideas from reading those papers.

Xavier: 00:53:51.905 That's great actually. Maybe that could be stuff to add to the reference section because yeah it's true that probably a bunch of stuff [inaudible].

Marc: 00:54:04.883 Well Remy, I also went to the IRC of this community that I am putting the link which is a community data that signs, and it might be something that will interest you. There are social scientist groups studying online communities and especially open source communities like Wikipedia, Linux, etc., from North American universities. I think you will be interested in that.

Rémi: 00:54:38.039 Thank you.

Marc: 00:54:40.431 And there may be also some papers you wrote before then. I'm not sure.

Rémi: 00:54:48.015 And we should [locally?] maybe contact some of the authors we find in those papers just to let them know we are working on that.

Marc: 00:55:02.386 I did mention my MOOC on their discussion channel.

Rémi: 00:55:09.331 Okay nice.

Xavier: 00:55:11.975 That's great. By the way it's not directly related, but it made me think right now. I've talked again on the Open edX and edX side about progress and etc. And so I didn't have a lot of reaction originally from edX themselves because I think they are just letting things happen. But I synced up a little bit more precisely with them. They will look at it a little bit more precisely. They will also talk to the people on the course content on edX, especially people who are maybe your project manager, who I don't know who that is, to make sure that we all are talking within the same loop. So just to let you know that you might hear about that through that group over there.

Rémi: 00:56:11.044 Thank you. I know them a little bit I think. I've been to edX multiple times. And I communicate with the-- I visited them multiple times also. And I communicated with the tech team really less often than you, but.

Xavier: 00:56:32.239 That's perfect. Yeah, I think they're really like that this is happening and etc. It's just to make sure. Because sometimes edX you will have on one side some people doing one thing and the other side completely ignoring each other, so not intentionally but just because of size. So I thought it would be good. I think that everyone will accept that, especially because we still have to discuss the parts related to business model, publication of the course, running the course, and different modes and etc. So actually, I don't know when and where you want to have that discussion, but that might loop in because edX will be fine in the middle of all that in any case. So, yeah.

Rémi: 00:57:15.033 All right. Maybe we will do another meeting after the brainstorming sessions.

Xavier: 00:57:23.090 Okay, yeah. Perfect.

Rémi: 00:57:25.321 Yeah, and I was thinking about very, very big projects having multiple communities. How do we deal with that? I am thinking about, let's say, Android is very big or big Ubuntu, any operating system. Sometimes it is confusing because people think when they use software into Ubuntu, is Ubuntu open to that [inaudible]. Sometimes it's a GNOME, a completely different team. And find a project and get it touch with the community can be tricky sometimes if one name of a project means a lot. And even for Ubuntu you have different communities around it, even different flavors of Ubuntu for French community, English community, I don't know.

Loïc: 00:58:27.656 It's true, it's a problem. But in the end, the question is, is it a social problem? Here is why I wonder because, say I want to contribute something, say it's a bug fix. I will contribute where it is the easiest to contribute to, regardless of where it actually is. For instance, say I want to fix a bug in a software that is in Ubuntu. If it's difficult to fix it upstream, maybe because upstream does not exist anymore, maybe I will fix it in Launchpad, so in Ubuntu. So maybe the idea is to convey that there might be multiple places where the contribution can happen on a given software. But how is it social in nature? That's what I don't see.

Rémi: 00:59:27.973 And also exactly you said that even at edX, part of the team doesn't know what happens in the other part of the team if something happens because it's too big.

Xavier: 00:59:39.202 Yeah. Any organization past a certain size, this of course happens. And communities especially with loosely linked people sometimes, you can definitely have things happening without other people knowing, so.
Yeah, at least being aware of that and trying to understand the scope of the part of the community someone is interacting might be important, yeah. Especially because you have that-- yeah, you have that a lot with distribution, I guess, because we have just talked about the example of Ubuntu, but you might have that with a project that has several parts. So if you have [inaudible] is I don't know with the Windows version, then it might be a completely different team, this kind of thing, so yeah.

Rémi: 01:00:28.532 Yes.

Loïc: 01:00:30.627 Yeah, true. I see there is diversity mentioned.

Rémi: 01:00:49.098 Yes.

Xavier: 01:00:54.263 I understood that as being part of the code of conduct part, not excluding minorities or something like that, but maybe it meant more than this.

Loïc: 01:01:11.038 Ah, Mark is back.

Marc: 01:01:13.525 Sorry, my browser crashed.

Loïc: 01:01:16.599 So we were asking, what was the idea behind diversity in this part. What did you have in mind?

Marc: 01:01:28.234 I had meant that we have to talk about the diversity or lack thereof in the open-source world and also of the diversity-- I mean, in the diversity of people contributing with a very high proportion of white males in their 30s from Europe and North America.

Xavier: 01:01:59.483 I don't see what you're talking about. We are not [inaudible] that matter at all.

Loïc: 01:02:05.571 That's a very good point. I would say a shining example of that would be exactly [crosstalk]--

Marc: 01:02:13.373 This panel?

Loïc: 01:02:16.563 Yeah, this panel. And then how to address that.

Marc: 01:02:22.610 Exactly. That was the point of me putting that here.

Loïc: 01:02:26.906 And how to address it should be applied to this course. So, of course. Otherwise--

Marc: 01:02:35.486 Yeah, of course. We should not have only typical members speaking about open source in our people we interview, for instance. We should strive to have some diversity of people we interview.

Loïc: 01:02:50.034 But [quickly?], as a free software project, how do we do that? On this course, how do we do it?

Marc: 01:03:01.077 I think we can try to address it by striving to be inclusive like having a code of conduct, this kind of stuff. Because part of the problem is this culture that was, until recently, very, very dominant of tech-focused and code-focused problem culture where if you're not contributing code, then you're worth less. So the lack of diversity from the tech world reflects in the open-source community and is even bigger in the open-source community because people are volunteers, mostly. So if you are not feeling welcome, you're not staying because you are paid for to be here. You're just not staying. So a problem with communities will increase the diversity problems that you already find in the pool of potential contributors.

Loïc: 01:04:23.696 Yeah, I agree that it is amplified by the volunteer nature of the work. One thing we could do--

Marc: 01:04:30.157 And also the volunteer nature of open source contributions amplifies problems of time-sharing in our culture basically, where men have men have more free time than women.

Loïc: 01:04:52.886 But we're not in a good situation. What we could do about it is act on it. So it's something quite-- we could do something like calling explicitly for people who are underrepresented. Though I don't think that was done in the last call for these meetings. But I've seen good results when you invite people for meetings like you did with PharmaSoft. It was excellent. And you explicitly say, "We are in a bad situation. Diversity-wise, we're not good. So please step up. Please come with us. Please help us fix it. If you have ideas, come." That is we acting on it can be as simple as trying to look for more diversity actively. Saying that diversity is needed. Saying that we have a problem. And it's not just a posture, of course. But maybe it's an easy start, and maybe we could do that.

Xavier: 01:06:09.656 And also another point is that because we are a course to introduce to contribution, we have a role to play in terms of trying to attract more diversity in the audience. Not just the people who make the course but the people who follow it. And especially because we are talking about parts of the course that might be paying like mentorship, certificates, these kind of things, maybe we could also include a scholarship where people from underrepresented group can apply to that and have it because we are trying to attract people [inaudible].

Marc: 01:06:51.569 I was speaking with the Outreachy project few days ago because Inkscape is entering the Outreachy program, and maybe we can ask them to give us advice on that. Outreachy is a program about onboarding an underrepresented group in internships in open-source projects, so.

Rémi: 01:07:15.873 I did some research on welcoming diversity for online courses. And especially a research on the first impression that you have when you enter a community or when you see a course. The very first impression you have even before you go to the course is very important in terms of how it will influence you for the subsequent behavior, or in the activity you will have to do. It means that if your first impression as a woman is bad, but you have to define why it is bad, then of course you will be less acting and less doing stuff in the course. So there is evidence of that. And some very interesting solutions use, how can I explain that, explicit of course as you said [inaudible] explicit welcoming for these kind of people. And also different sentences or different images, sometime they have also an impact of how you feel welcome in the course or how you feel welcome to do something.

Loïc: 01:08:51.879 Yeah. And it's very interesting to see a project dated 10 years ago, and some of them are-- I don't have an example in mind, but some of them look a lot better now. And if you look back you say, "Oh, that was not very welcoming to diversity," and of course what comes to mind, but it's difficult to insert in a course, is where you had too many sexualized content or dialogues and that is was tolerated, even applauded. And this is the kind of behavior that makes everyone uncomfortable, but specially minorities of course. So that changed over time and now we have much easier-- I'm very old, so I've been in a world before everything when patriarchy was the dominant [inaudible] we were kings. This we're no longer.

Marc: 01:10:00.998 And FLOSS was a boy's club, basically.

Loïc: 01:10:03.663 Yes. So I saw that transformation and it's spectacular. So my point is 40 years ago, I would have spoken in a way that was hostile to minorities without realizing it. And nowadays I learn what is acceptable. I learn to behave socially and to be welcoming. So it's easier now to write down a course or website with the language that is welcoming. But still what I also learned is because I'm part of this former majority and part of the problem, I cannot do that. I must not be the one doing that. It's completely wrong. So as a group of four people here, we must not be the one addressing the problem. The only thing we can do is increase diversity so we don't have to because we can't.

Marc: 01:11:19.949 And the other point about diversity that I wanted to talk about was the diversity of contributors within a project and the relationships within them or between them.

Loïc: 01:11:32.321 Oh, you mean [inaudible] from a company.

Marc: 01:11:36.914 Not only that but also the types of contributors. Yes, there is a type, like a paid contributor from a company or a volunteer contributor. But also basically UX designers or people who just help on the forums or people who report bugs and cannot fix them because they are not coders etc. and how each one treat one another, basically. And the fact that it used to be that the people coding were basically kings and could treat others-- well, it's hard to say, "Could treat others badly," but it correspond to some kind of reality, and.

Loïc: 01:12:23.735 I agree that is--

Rémi: 01:12:25.640 You want the MOOC to be courteous and contributing to code, right?

Marc: 01:12:29.859 Yeah. But I meant also-- I want to say that a project is not just about code. It's about the community before being about the code. And you have to, basically, respect that there are other people that are expert in other things than code, and they are as important-- they are also important to the project.

Loïc: 01:12:50.883 But how does that relate to the contributor? I see how it is [crosstalk]--

Marc: 01:12:56.465 You can make, for instance, a merge request as an attempt to fix something, and you have someone who he's a UX designer/contributor to the project and comes and say, "This feature is not really user-friendly. It could be improved in the following ways, and let's do a [user's study?] first," in the best of case where there are UX contributors in the project.

Loïc: 01:13:21.461 A very good point, yes, yes.

Marc: 01:13:22.761 And there could be some pushbacks from contributors where they say, "Oh, you are just a UX contributors, but I've made something that works." And this is a bad reaction, for instance.

Loïc: 01:13:40.743 I assume you know about opensourcedesign.net?

Marc: 01:13:45.068 Yes.

Loïc: 01:13:45.424 Yes. So they are a shining example that you can show, yeah. But there are others. You're right, yeah.

Marc: 01:13:54.837 So that was for the list of things that I wanted to mention this week.

Rémi: 01:14:03.137 I think it's almost time. I mean, time 6 past 8:00 already. And we've been discussing a lot. As always, it goes so fast. It is so interesting. We are so into it that we don't see the time passing so fast. It's incredible.

Loïc: 01:14:20.260 It's supposed to be, what, 60 minutes?

Marc: 01:14:24.179 No, it's supposed to be from 6:30 French time to 8:00 French time.

Xavier: 01:14:35.176 Oh, and by the way, for the transcripts from the previous sessions, I've started that. [I've worked with them?]. So hopefully, sometime this week we should be getting the transcript from the previous sessions.

Rémi: 01:14:49.727 Very nice. Thank you. And do you do that automatically first, and then do a human pass on that, or?

Xavier: 01:14:58.277 No, purely human. It's actually a two-pass, two different people doing it because I've tried the automatic one, and it's so often really bad, especially with our accents and the technical terms and etc. Usually half of it is completely unintelligible. Yes, sorry. I include myself, definitely, in that. So yeah, I do it through transcribeme.net, something like that.

Rémi: 01:15:25.797 Wow. Thank you very much for that. It will be very [crosstalk]--

Xavier: 01:15:30.956 Oh, sure. I mean, there are so many good and interesting things said during those meetings that I think it's going to be good to have them all written, so yeah.

Marc: 01:15:42.586 Yeah. If only to make the contents of the-- to make a proposal and the contents of each week, we will need to go back maybe several times to the transcript. So it was a time to transcript, definitely, I think.

Rémi: 01:16:03.453 Well, I think it's time to conclude.

Marc: 01:16:06.860 Yep. So this is, basically, the last week talking about the social aspects of contributions, since next two weeks are on tools and the ways to-- so next week is, basically, tools about how to build the test and do steps that is done in big projects but is not special to open-source projects. And the next one is really about diving in existing big codebases, which is sort of a special skill that is not often taught and is quite necessary to go into many open-source projects. So, yeah, this one was last social week. So the students should finish this week and know how to interact with people, basically. And after that, we can come to the code stuff.

Rémi: 01:17:25.464 All right. So next brainstorming session will be, when exactly? 20 [crosstalk]--

Xavier: 01:17:34.700 Thursday, I think. It's like the other weeks.

Rémi: 01:17:39.098 Thursday, yes.

Xavier: 01:17:40.066 And Wednesday, we have the meeting with the OpenStack.

Rémi: 01:17:46.154 Right. Well, thank you all for this brainstorming session.

Marc: 01:17:50.659 Thank you everyone for attending, yeah?

Rémi: 01:17:52.809 Yeah. And we hope to see you at the next sessions.

Xavier: 01:17:57.698 Yep. See you soon.

Marc: 01:17:59.049 See you. Bye.

Loïc: 01:17:59.274 Bye-bye.

Rémi: 01:18:00.899 Okay, thank you.
